# import numpy as np
import cv2
import socket
import threading
import time


class MotionDetector:

    def __init__(self, skt, camera_source=0, show_stream=True):
        self.receiver_socket = skt

        self.sender_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        port = 5001
        self.sender_socket.bind(("", port))

        self.continue_stream = True
        self.shutdown = False
        self.camera_source = camera_source
        self.show_stream = show_stream
        self.cap = None
        self.previous_frame = None
        self.clients = []
        self.listen_thread = None
        self.cool_down = True

    def simple_motion_detector(self, current):
        if sum(sum(sum(current - self.previous_frame))) > 650:
            # 3 sums to squash 3 dimensional array into a single number
            # The number 650 is set by tuning and can be further fine tuned, range between 600 and 700
            # print("Motion Detected")
            return True
        return False

    def run(self, client_address):
        self.continue_stream = True
        self.listen_thread = threading.Thread(target=self.listener)
        self.listen_thread.start()

        # Populating the list of Clients
        self.clients.append(client_address[0])

        # The parameter 0 means capture video from the "WebCam"
        self.cap = cv2.VideoCapture(self.camera_source)

        # Initialization of previous frame to be used inside the while loop
        _, self.previous_frame = self.cap.read()

        while self.continue_stream:
            # Capture frame-by-frame
            ret, frame = self.cap.read()

            # Check if a motion is detected
            if self.cool_down and self.advanced_motion_detector(frame):
                print("Motion Detected")
                self.sender()
                cool_down_thread = threading.Thread(target=self.cool_down_timer)
                cool_down_thread.start()

            # update previous frame
            self.previous_frame = frame.copy()

            if self.show_stream:
                # Display the resulting frame "only for testing"
                cv2.imshow('frame', frame)

            # to quit press q (works only when show stream is activated)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                self.shutdown = True
                break

        # When everything done, release the capture
        self.cap.release()
        cv2.destroyAllWindows()
        return self.shutdown

    def listener(self):
        while self.continue_stream:
            try:
                data, address = self.receiver_socket.recvfrom(2048)
            except socket.timeout:
                continue
            if data.decode("UTF-8") == "Camera":
                self.clients.append(address[0])

            if address[0] in self.clients:
                if data.decode("UTF-8") == "Soft Exit":
                    print(data.decode("UTF-8"))
                    self.clients.remove(address[0])
                    if not len(self.clients):
                        self.continue_stream = False
                elif data.decode("UTF-8") == "Hard Exit":
                    print(data.decode("UTF-8"))
                    self.continue_stream = False
                    self.shutdown = True

    def sender(self):
        subscribers = [x for x in self.clients]
        for address in subscribers:
            self.sender_socket.sendto("Motion Detected".encode("UTF-8"), (address, 5001))

    def cool_down_timer(self):
        self.cool_down = False
        time.sleep(10)
        self.cool_down = True
        print("Ready For more detection ...")

    def advanced_motion_detector(self, current):
        # Convert both frames to Gray-scale for further processing
        prev_gray = cv2.cvtColor(self.previous_frame, cv2.COLOR_BGR2GRAY)
        current_gray = cv2.cvtColor(current, cv2.COLOR_BGR2GRAY)

        # Apply a Blurring algorithm (eg: Gaussian Blur) to get rid of extreme pixel values due to noisy camera sensors
        prev_gray = cv2.GaussianBlur(prev_gray, (19, 19), 0)
        current_gray = cv2.GaussianBlur(current_gray, (19, 19), 0)

        # Get the difference in pixel values between the two frames (Absolute difference)
        delta = cv2.absdiff(current_gray, prev_gray)
        # Threshold the resulting difference to get rid in small changes in pixels between still images (Tunable)
        thresh = cv2.threshold(delta, 25, 255, cv2.THRESH_BINARY)[1]

        # Dilate the resulting image to connect white pixels of the same object
        thresh = cv2.dilate(thresh, None, iterations=2)

        # Use Finding Contours to find objects in the difference image (means that object moved hence motion detected)
        _, contours, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # If there is any element found (any object moved) we should return true
        # But First let's build so tolerance to small changes
        for c in contours:
            # Any very small object in a 400 pixel area(eg:20px20p) should be considered movement just noise or shadows
            if cv2.contourArea(c) < 400:
                continue
            return True
        return False
