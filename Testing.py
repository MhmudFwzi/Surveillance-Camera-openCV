import socket
import threading

keep_going = True


def listen():
    receive_skt = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    receive_skt.settimeout(30)
    port = 6001
    receive_skt.bind(("", port))
    while keep_going:
        # print("Listening on Port: " + str(port))
        try:
            data, address = receive_skt.recvfrom(2048)
            message = data.decode("UTF-8")
            print("Message received from: ", address)
            print(message)
        except socket.timeout:
            continue


th = threading.Thread(target=listen)
th.start()

send_skt = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
send_skt.bind(("", 6000))

other_address = ("localhost", 5000)

while True:
    m = input("Data to be sent: ")
    if m == "quit":
        keep_going = False
        break
    send_skt.sendto(m.encode("UTF-8"), other_address)
