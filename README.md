# Surveillance-Camera-openCV

An implementation of a surveillance camera using OpenCV library. The camera should notify a mobile application to activate an alarm in case a motion was detected.


## Credits

* محمود عبد المحسن محمد هلال عطية&emsp;&emsp;&emsp;ID: 1301272 &emsp;&emsp;BN: 43834
* محمود فوزى جمال الدين إبراهيم&emsp;&emsp;&emsp;&emsp;ID: 1301278 &emsp;&emsp;&emsp;&emsp;BN: 43835
* محمود محمد صلاح الدين محمد امام&emsp;&emsp;&emsp;ID: 1301287 &emsp;&emsp;BN: 43837
* مصطفى محمد مصطفى زاهر&emsp;&emsp;&emsp;&emsp;&emsp;ID: 1301361 &emsp;&emsp;&emsp;&emsp;BN: 43844


## Demo

<b>Check the video Demo of the project working</b> [here] (https://drive.google.com/drive/folders/1pUC7_7jqRVTb4Hm8fcnUT9E96wB7B1Hu?usp=sharing)

## How to use

Note: This Repo Contains both parts of the project (The Python server script and The Android application)
* Install the apk on an Android phone and run the application
* Run Python script "Main.py" while making sure the file "Surveillance_Camera.py" is in the same directory
* Enter the ip address of the Python server host into the application and login


## Python Part: Dependences

* Python 3
* Numpy
* OpenCV (for Python 3)


## Android Part: Building

* You can either use the apk provided or build it from scratch using the source code files in the repo.
* Any updates in the source files we'll provide the modified apk version after building it.

## Features

* The Script acts as a server waiting for incoming Connections
* More than One device (On many different Android devices) can be connected at once 
* when login is established WebCam is used to capture video
* Motion detection Algorithm is used to decide whether there is a motion or not
* All subscribed devices will get notified (an Android Notification will pop up in the connected devices)
* LOGOUT: User can use application remotely to close camera and wait for other connections (if no other device is connected)
* CLOSE SERVER: User can use application remotely to Shutdown the whole script/server (must be Logged in first)