from Surveillance_Camera import MotionDetector
import socket

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

host = ""
port = 5000

udp_socket.bind((host, port))
udp_socket.settimeout(30)

motion_detector = MotionDetector(udp_socket, camera_source=0, show_stream=True)

shutdown = False
while not shutdown:
    print("The server is ON, Ready for incoming requests on port: " + str(port))
    try:
        data, clientAddress = udp_socket.recvfrom(2048)
    except socket.timeout:
        continue
    message = data.decode("UTF-8")
    if message == "Camera":
        shutdown = motion_detector.run(clientAddress)
