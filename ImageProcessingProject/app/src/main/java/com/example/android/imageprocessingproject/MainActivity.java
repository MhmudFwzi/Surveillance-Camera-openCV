package com.example.android.imageprocessingproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {

    private int client_port = 5000;
    private int server_port = 5001;
    private DatagramSocket cientSocket = null;
    private String ip_address = "";
    private startListening listener = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button loginButton = findViewById(R.id.login_button);
        final Button logoutButton = findViewById(R.id.logout_button);
        final Button closeServerButton = findViewById(R.id.close_server);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText et = findViewById(R.id.ip_address);
                String ip = et.getText().toString();
                if (checkIPAvailability(ip)) {
                    ip_address = ip;
                    Log.e("Akkked", "msh hna");
                    closeServerOrCamera("Camera");
                    et.setEnabled(false);
                    loginButton.setEnabled(false);
                    logoutButton.setEnabled(true);
                    closeServerButton.setEnabled(true);
                    Toast toast = Toast.makeText(getApplicationContext(), "Login Successful",Toast.LENGTH_SHORT);
                    toast.show();
                    listener = new startListening();
                    listener.execute();
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "IP is invalid", Toast.LENGTH_SHORT);
                    toast.show();
                }

                }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeServerOrCamera("Soft Exit");
                EditText et = findViewById(R.id.ip_address);
                et.setEnabled(true);
                loginButton.setEnabled(true);
                logoutButton.setEnabled(false);
                closeServerButton.setEnabled(false);
                Toast toast = Toast.makeText(getApplicationContext(), "Logout Successful",Toast.LENGTH_SHORT);
                toast.show();
                listener.cancel(true);
                Log.e("Tag", listener.getStatus().toString());
            }
        });

        closeServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("TAG", "eeeeeeeeeeeeh");
                closeServerOrCamera("Hard Exit");
                EditText et = findViewById(R.id.ip_address);
                et.setEnabled(false);
                loginButton.setEnabled(false);
                logoutButton.setEnabled(false);
                Toast toast = Toast.makeText(getApplicationContext(), "Server is Closed",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    @Override
    protected void onPause() {
        if(this.isFinishing())
            closeServerOrCamera("Hard Exit");
        super.onPause();
    }

    private void closeServerOrCamera(final String command)
    {
        new Thread(new Runnable() {
            public void run() {
                // a potentially  time consuming task
                DatagramSocket clientSocket = null;

                try {
                    Log.e("TAG", "eeeeeeeeeeeeh tany");
                    InetAddress ipAdress        = InetAddress.getByName(ip_address);
                    clientSocket = new DatagramSocket();
                    clientSocket.setBroadcast(true);
                    clientSocket.setSoTimeout(10000);
                    String sendString           = command;
                    DatagramPacket packet       = new DatagramPacket(sendString.getBytes(), sendString.length(), ipAdress, client_port);
                    clientSocket.send(packet);
                    Log.e("SENT", "Hard Exit");
                    String getData = new String(packet.getData());
                    Log.e("out", "byeeeeeeeee");

                } catch(SocketException e)
                {
                    e.printStackTrace();
                    String error = e.toString();
                    Log.e("Error by Sender", error);
                }
                catch(UnknownHostException e)
                {
                    e.printStackTrace();
                    String error = e.toString();
                    Log.e("Error by Sender", error);
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                    String error = e.toString();
                    Log.e("Error by Sender", error);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    String error = e.toString();
                    Log.e("Error by Sender", error);
                }
                finally{
                    if(clientSocket != null){
                        //clientSocket.close();
                    }
                }
            }
        }).start();
    }

    private class logInOut extends AsyncTask<String, Void, Void>
    {

        @Override
        protected Void doInBackground(String... params) {
            DatagramSocket clientSocket = null;

            try {
                Log.e("TAG", "eeeeeeeeeeeeh tany");
                InetAddress ipAdress        = InetAddress.getByName(ip_address);
                clientSocket = new DatagramSocket();
                clientSocket.setBroadcast(true);
                clientSocket.setSoTimeout(10000);
                String sendString           = params[0];
                DatagramPacket packet       = new DatagramPacket(sendString.getBytes(), sendString.length(), ipAdress, client_port);
                clientSocket.send(packet);
                Log.e("SENT", params[0]);
                String getData = new String(packet.getData());
                Log.e("out", "byeeeeeeeee");

            } catch(SocketException e)
            {
                e.printStackTrace();
                String error = e.toString();
                Log.e("Error by Sender", error);
            }
            catch(UnknownHostException e)
            {
                e.printStackTrace();
                String error = e.toString();
                Log.e("Error by Sender", error);
            }
            catch(IOException e)
            {
                e.printStackTrace();
                String error = e.toString();
                Log.e("Error by Sender", error);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                String error = e.toString();
                Log.e("Error by Sender", error);
            }
            finally{
                if(clientSocket != null){
                    //clientSocket.close();
                }
            }
            return null;
        }
    }

    private class startListening extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            byte[] receiveData = new byte[2048];

            try {
                Log.e("Listen", "Start Listening");
                DatagramSocket serverSocket = new DatagramSocket(server_port);
                serverSocket.setSoTimeout(10000);
                while (!isCancelled()) {
                    DatagramPacket packet = new DatagramPacket(receiveData, receiveData.length);
                    Log.e("Listen", "Socket and Packet are done");
                    try {
                        serverSocket.receive(packet);
                        Log.e("Listen", "Packet Recieved");
                        String getData = new String(packet.getData());
                        String msg = getData.substring(0, "Motion Detected".length());
                        Log.e("out", msg);
                        if (msg.equals("Motion Detected")) {
                            pushNotification();
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e("TAG", "TIMEOUT");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            listener = null;
            Log.e("TAG", "7d  b=augfjqajfk");
        }
    }

    private void pushNotification()
    {
        Log.e("Push", "hapush aho");
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(),
                (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "MY_CH_ID")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Surveillance Camera")
                        .setContentText("Motion detected in your Room!")
                        .setContentIntent(pIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
    }


    private boolean checkIPAvailability(String ip)
    {
        if(ip.isEmpty())
            return false;
        String ip2 = ip.replace(".", "");
        if(!TextUtils.isDigitsOnly(ip2))
            return false;
        String[] ip3 = ip.split("\\.");
        if(ip3.length != 4)
            return false;
        for (int i=0;i<ip3.length;i++)
        {
            if(Integer.parseInt(ip3[i]) > 255)
                return false;
        }
        return true;
    }

}
